﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SoundService
{
    public partial class SoundService : ServiceBase
    {
        public SoundService()
        {
            InitializeComponent();
            sound = new SoundWorker();
            server = new ServerWorker(7777, sound);
        }

        private SoundWorker sound;
        private ServerWorker server;
        protected override void OnStart(string[] args)
        {
            try
            {
                server.StartServer();
                sound.Start();
                Logger.For(this).Info(DateTime.UtcNow.ToShortDateString() + ": " + "Server Start");
            }
            catch (Exception ex)
            { 
                Logger.For(this).Info(DateTime.UtcNow.ToShortDateString() + ": " + ex.Message);
            }
           
        }

        protected override void OnStop()
        {
            sound.Stop();
            server.StopServer();
            Logger.For(this).Info(DateTime.UtcNow.ToShortDateString() + ": " + "Server is stopped!");
        }
    }
}
