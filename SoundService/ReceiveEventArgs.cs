﻿using System;
using System.ComponentModel;
using System.IO;

namespace SoundService
{
    public delegate void ReceiveEventHandler(object sender,
        ReceiveEventArgs e);

    public class ReceiveEventArgs : EventArgs
    {
        public ReceiveEventArgs(MemoryStream data)
        {
            Data = data;
        }

        public MemoryStream Data { get; }
    }
}