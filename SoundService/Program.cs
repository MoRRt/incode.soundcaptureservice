﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SoundService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            if (Environment.UserInteractive)
            {
                SoundWorker sound = new SoundWorker();
                ServerWorker server= new ServerWorker(7777,sound);

                server.StartServer();
                sound.Start();

                Console.WriteLine("The server started successfully, press key 'q' to stop it!");

                while (Console.ReadKey().KeyChar != 'q')
                {
                    Console.WriteLine();
                }

                sound.Stop();
                server.StopServer();
            }
            else
            {
                ServiceBase[] servicesToRun = {new SoundService()};
                ServiceBase.Run(servicesToRun);
            }
        }
    }
}
