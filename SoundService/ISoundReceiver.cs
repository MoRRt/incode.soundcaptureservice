﻿namespace SoundService
{
    public interface ISoundReceiver
    {
        void Start();
        void Stop();
        event ReceiveEventHandler Receive;
    }
}