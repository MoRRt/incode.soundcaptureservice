﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio.Lame;
using NAudio.Wave;

namespace SoundService
{
    public class SoundWorker : ISoundReceiver
    {
        private LameMP3FileWriter wri;
        private bool stopped = false;
        private IWaveIn waveIn;
        public event ReceiveEventHandler Receive;
        private MemoryStream streamOut;

        public SoundWorker()
        {
            try
            {
                // Start recording from loopback
                waveIn = new WasapiLoopbackCapture();
                waveIn.DataAvailable += waveIn_DataAvailable;
                waveIn.RecordingStopped += waveIn_RecordingStopped;
                // Setup MP3 writer to output at 32kbit/sec (~2 minutes per MB)
                stopped = false;
            }
            catch (Exception ex)
            {
                Logger.For(this).Info(DateTime.UtcNow.ToShortDateString() + ": " + ex.Message);
            }
        }

        #region Events

        void waveIn_RecordingStopped(object sender, StoppedEventArgs e)
        {
            // signal that recording has finished
            stopped = true;
        }

        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            // write recorded data to MP3 writer
            wri?.Write(e.Buffer, 0, e.BytesRecorded);
            if (streamOut.Length != 0)
            {
                OnReceive(new ReceiveEventArgs(streamOut));
            }
        }
        #endregion

        public void Start()
        {
            streamOut = new MemoryStream();
            wri = new LameMP3FileWriter(streamOut, waveIn.WaveFormat, 32);
            waveIn.StartRecording();
        }

        public void Stop()
        {
            waveIn.StopRecording();
            if (wri != null)
            {
                wri.Flush();
                wri.Dispose();
            }
        }

        protected virtual void OnReceive(ReceiveEventArgs e)
        {
            Receive?.BeginInvoke(this, e, ar =>
            {
                streamOut.Seek(0, SeekOrigin.Begin);
                streamOut.SetLength(0);
            }, null);
        }
    }
}