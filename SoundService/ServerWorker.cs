﻿using System;
using System.Collections.Generic;
using SuperSocket.SocketBase.Logging;
using SuperWebSocket;
using SuperWebSocket.SubProtocol;

namespace SoundService
{
    public class ServerWorker
    {
        private ISoundReceiver soundReciever;
        private int Port { get; }
        private WebSocketServer ws;
        public ServerWorker(int port, ISoundReceiver soundReciever)
        {
            Port = port;
            this.soundReciever = soundReciever;
            soundReciever.Receive += Broadcast;
            ws = new WebSocketServer();
        }

        public void StartServer()
        {
            if (ws == null && !ws.Setup(Port)) //Setup with listening port
            {
                Logger.For(this).Info("Port failed!");
                Console.WriteLine("Port failed!");
            }
            else
            {
                ws.Setup(Port);
                ws.Start();
            }

        }

        public void StopServer()
        {
            ws.Stop();
        }
        public void Broadcast(object sender, ReceiveEventArgs e)
        {
            IEnumerable<WebSocketSession> sessions = ws?.GetAllSessions();
            foreach (WebSocketSession session in sessions)
            {
                try
                {
                    lock (session)
                    {
                        var array = e.Data.ToArray();
                        session.Send(array, 0, array.Length);
                    }
                }
                catch (Exception ex)
                {
                    Logger.For(this).Info(DateTime.UtcNow.ToShortDateString() +": "+ ex.Message);
                }
            }
        }
    }
}